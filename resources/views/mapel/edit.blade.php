@extends('layout.master')
@section('title', 'Mapel | Edit')
@section('content')
<div class="page-header">
    <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white mr-2">
        <i class="mdi mdi-file-document-box"></i>
      </span> Mata Pelajaran
    </h3>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Mata Pelajaran</a></li>
          <li class="breadcrumb-item active" aria-current="page">Edit Data</li>
        </ol>
      </nav>
  </div>

  <div class="grid-margin stretch-card">
    <div class="card">
        <div class="card-header">
          <h4 class="card-title">Edit Data Mata Pelajaran</h4>
        </div>
        <div class="card-body">
            <form action="{{ route('mapel.update',$mapel->id_mapel) }}" method="post" enctype='multipart/form-data'>
                @csrf
                <div class="form-group row">
                  <label for="exampleInputUsername2" class="col-sm-3 col-form-label">ID Mapel</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" disabled name="id_mapel" id="exampleInputUsername2"
                    value="{{ $mapel->id_mapel}}">
                  </div>
                </div>
            <div class="form-group row">
                <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Nama Mapel</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="nama_mapel" id="exampleInputUsername2"
                  value="{{ $mapel->nama_mapel}}">
                </div>
              </div>

            <div class="form-group row">
                <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Guru</label>
                <div class="col-sm-9">
                    <select class="col-sm-5 form-control" name="nip" id="example-text-input">
                        <option value="">Pilih Guru</option>
                        @foreach ($guru as $data)
                        <option value="{{ $data->nip }}" {{ $data->nip == $mapel->nip ? 'selected' : '' }}>
                            {{ $data->nama_guru }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

        </div>
        <div class="card-footer">
            <button type="submit" class='btn btn-success float-right'>Simpan</button>
        </div>
    </div>
</form>
  </div>
@endsection
