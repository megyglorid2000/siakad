@extends('layout.master')
@section('title', 'Mapel | Tambah')
@section('content')
<div class="page-header">
    <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white mr-2">
        <i class="mdi mdi-file-document-box"></i>
      </span> Mata Pelajaran
    </h3>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Mata Pelajaran</a></li>
          <li class="breadcrumb-item active" aria-current="page">Tambah Data</li>
        </ol>
      </nav>
  </div>

  <div class="grid-margin stretch-card">
    <div class="card">
        <div class="card-header">
          <h4 class="card-title">Tambah Data Mata Pelajaran</h4>
        </div>
        <div class="card-body">
            <form action="{{ route('mapel.store') }}" method="post" enctype='multipart/form-data'>
                @csrf
            <div class="form-group row">
                <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Nama Mapel</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="nama_mapel" id="exampleInputUsername2" placeholder="Masukan Nama Mata Pelajaran">
                </div>
            </div>

            <div class="form-group row">
                <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Guru</label>
                <div class="col-sm-9">
                    <select class="col-sm-5 form-control" name="nip" id="example-text-input">
                        <option value="">Pilih Guru</option>
                        @foreach ($guru as $data)
                        <option value="{{ $data->nip }}">{{ $data->nama_guru }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

        </div>
        <div class="card-footer">
            <button type="submit" class='btn btn-success float-right'>Tambah</button>
        </div>
    </div>
</form>
  </div>
@endsection
