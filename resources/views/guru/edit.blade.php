@extends('layout.master')
@section('title', 'Siakad | Guru')

@section('content')
<div class="page-header">
    <h3 class="page-title">
        <span class="page-title-icon bg-gradient-primary text-white mr-2">
            <i class="mdi mdi-account-card-details menu-icon"></i>
        </span> Data Guru
    </h3>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Guru</a></li>
            <li class="breadcrumb-item active" aria-current="page">Tambah Data Guru</li>
        </ol>
    </nav>
</div>

<div class="grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <form action="{{ route('guru.update', $guru->nip) }}" method="post" enctype='multipart/form-data'>
                @csrf
                <div class="form-group">
                    <label>NIP</label>
                    <input type="text" class="form-control" autocomplete="off" name="nip" value="{{ $guru->nip }}">
                </div>
                <div class="form-group">
                    <label>Nama Lengkap</label>
                    <input class="form-control" autocomplete="off" name="nama_guru" value="{{ $guru->nama_guru}}">
                </div>
                <div class="form-group">
                    <label>Jenis Kelamin</label>
                    <select class="form-control" name="jenis_kelamin">
                        <option >{{$guru->jenis_kelamin}}</option>
                        <option value="Laki Laki">Laki Laki</option>
                        <option value="Perempuan">Perempuan</option>
                    </select>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Tempat Lahir</label>
                            <input class="form-control" autocomplete="off" name="tempat_lahir" value="{{ $guru->tempat_lahir }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Tanggal Lahir</label>
                            <input type="text" id="datepicker" class="form-control" autocomplete="off" name="tanggal_lahir"
                                value="{{ $guru->tanggal_lahir}}">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Alamat</label>
                    <textarea class="form-control" autocomplete="off" name="alamat"
                        value="{{ $guru->alamat }}">{{ $guru->alamat }}</textarea>
                </div>
                <div class="form-group">
                    <label>Telepon</label>
                    <input class="form-control" autocomplete="off" name="telepon" value="{{ $guru->telepon }}">
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Agama</label>
                            <input class="form-control" autocomplete="off" name="agama" value="{{ $guru->agama}} ">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Pendidikan</label>
                            <input class="form-control" autocomplete="off" name="pendidikan" value="{{ $guru->pendidikan }}">
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-gradient-primary mr-2 float-right">Simpan</button>
        </div>
    </div>
    </form>
</div>
@endsection()
@section('script')
<script>
    var picker = new Pikaday({
        field: document.getElementById('datepicker'),
        format: 'D/MM/YYYY',
        toString(date, format) {
            // you should do formatting based on the passed format,
            // but we will just return 'D/M/YYYY' for simplicity
            const day = date.getDate();
            const month = date.getMonth() + 1;
            const year = date.getFullYear();
            return `${day}/${month}/${year}`;
        }
    });

</script>
@endsection
