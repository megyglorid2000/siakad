@extends('layout.master')
@section('title', 'Siswa | Edit')
@section('content')
<div class="page-header">
    <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white mr-2">
        <i class="mdi mdi-file-document-box"></i>
      </span> Siswa
    </h3>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Siswa</a></li>
          <li class="breadcrumb-item active" aria-current="page">Tambah Data</li>
        </ol>
      </nav>
  </div>

  <div class="grid-margin stretch-card">
    <div class="card">
        <div class="card-header">
          <h4 class="card-title">Tambah Data Siswa</h4>
        </div>
        <div class="card-body">
            <form action="{{ route('siswa.update',$siswa->nisn) }}" method="post" enctype='multipart/form-data'>
                @csrf

            <div class="form-group row">
                <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Nama</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="nama" id="exampleInputUsername2"  value="{{ $siswa->nama}}">
                </div>
            </div>

            <div class="form-group row">
                <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Jenis Kelamin</label>
                <div class="col-sm-9">
                    <select class="col-sm-5 form-control" name="jenis_kelamin" id="example-text-input">
                        <option >{{$siswa->jenis_kelamin}}</option>
                    <option value="Laki - Laki">Laki - Laki</option>
                    <option value="Perempuan">Perempuan</option>
                    </select>
                </div>
            </div>


            <div class="form-group row">
                <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Tempat Lahir</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="tempat_lahir" id="exampleInputUsername2" value="{{ $siswa->tempat_lahir}}">
                </div>
            </div>

            <div class="form-group row">
                <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Tgl Lahir</label>
                <div class="col-sm-9">
                  <input type="date" class="form-control" name="tanggal_lahir" id="exampleInputUsername2" value="{{ $siswa->tanggal_lahir}}">
                </div>
            </div>

            <div class="form-group row">
                <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Alamat</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="alamat" id="exampleInputUsername2" value="{{ $siswa->alamat}}">
                </div>
            </div>

            <div class="form-group row">
                <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Telepon</label>
                <div class="col-sm-9">
                  <input type="number" class="form-control" name="telepon" id="exampleInputUsername2" value="{{ $siswa->telepon}}">
                </div>
            </div>

            <div class="form-group row">
                <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Agama</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="agama" id="exampleInputUsername2" value="{{ $siswa->agama}}">
                </div>
            </div>

            <div class="form-group row">
                <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Nama Ayah</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="nama_ayah" id="exampleInputUsername2" value="{{ $siswa->nama_ayah}}">
                </div>
            </div>

            <div class="form-group row">
                <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Nama Ibu</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="nama_ibu" id="exampleInputUsername2" value="{{ $siswa->nama_ibu}}">
                </div>
            </div>

        </div>
        <div class="card-footer">
            <button type="submit" class='btn btn-success float-right'>Simpan</button>
        </div>
    </div>
</form>
  </div>
@endsection
