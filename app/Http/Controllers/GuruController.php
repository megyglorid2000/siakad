<?php

namespace App\Http\Controllers;
use App\Models\Guru;
use Illuminate\Http\Request;

class GuruController extends Controller
{
    public function index(Request $request) {
        $guru= Guru::get();
        return view('guru.index', compact('guru'));
    }

    public function create(){
        return view('guru.create');
    }

    public function store(Request $request)
    {
        // $request->validate(
        //     [
        //     'nip' => 'required|min:18|numeric',
        //     'nama_guru' => 'required',
        //     'jenis_kelamin' => 'required',
        //     'tempat_lahir' => 'required',
        //     'tanggal_lahir' => 'required',
        //     'alamat' => 'required',
        //     'telepon' => 'required|numeric|max:15',
        //     'agama' => 'required',
        //     'pendidikan' => 'required'
        //     ],
        //     [
        //         'nip.required' => 'NIP harus diisi',
        //         'nip.min' => 'NIP wajib 18 digit angka',
        //         'nip.numeric' => 'NIP berupa digit angka',
        //         'nama_guru.required' => 'Nama harus diisi',
        //         'jenis_kelamin.required' => 'Jenis Kelamin harus diisi',
        //         'tempat_lahir.required' => 'Tempat Lahir harus diisi',
        //         'tanggal_lahir.required' => 'Tanggal Lahir harus diisi',
        //         'telepon.required' => 'Telepon harus diisi',
        //         'telepon.numeric' => 'Telepon harus berupa digit angka',
        //         'telepon.max' => 'Telepon maksimal 15 digit',
        //         'agama.required' => 'Agama harus diisi',
        //         'pendidikan.required' => 'Pendidikan harus diisi'
        //     ]
        // );

        $guru = new Guru;
        $guru->nip = $request->nip;
        $guru->nama_guru = $request->nama_guru;
        $guru->jenis_kelamin = $request->jenis_kelamin;
        $guru->tempat_lahir = $request->tempat_lahir;
        $guru->tanggal_lahir = $request->tanggal_lahir;
        $guru->alamat = $request->alamat;
        $guru->telepon = $request->telepon;
        $guru->agama = $request->agama;
        $guru->pendidikan = $request->pendidikan;
        $guru->save();

        return redirect()->route('guru.index')->with('success', 'Data Berhasil Ditambah');
    }
    public function edit(Request $request, $nip){
        $guru = guru::findorFail($nip);
        return view('guru.edit', compact('guru'));
    }
    public function update(Request $request, $nip){
        $guru = guru::find($nip);

        $request->validate([
            'nip' => 'required'
        ]);

        $guru->nip = $request->nip;
        $guru->nama_guru = $request->nama_guru;
        $guru->jenis_kelamin = $request->jenis_kelamin;
        $guru->tempat_lahir = $request->tempat_lahir;
        $guru->tanggal_lahir = $request->tanggal_lahir;
        $guru->alamat = $request->alamat;
        $guru->telepon = $request->telepon;
        $guru->agama = $request->agama;
        $guru->pendidikan = $request->pendidikan;

        $guru -> update();
        return redirect()->route('guru.index');
    }
    public function destroy($nip){
        $guru = guru::find($nip);
        $guru -> delete();
        return redirect()->back();
    }
}
