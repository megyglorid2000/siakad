<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('kelas')->group(function () {
    Route::get('/', 'KelasController@index')->name('kelas.index');
    Route::get('/create', 'KelasController@create')->name('kelas.create');
    Route::post('/', 'KelasController@store')->name('kelas.store');
    Route::get('/edit/{id_kelas}', 'KelasController@edit')->name('kelas.edit');
    Route::post('/update/{id_kelas}', 'KelasController@update')->name('kelas.update');
    Route::post('/delete/{id_kelas}', 'KelasController@destroy')->name('kelas.destroy');
});

Route::prefix('guru')->group(function () {
    Route::get('/', 'GuruController@index')->name('guru.index');
    Route::get('/create', 'GuruController@create')->name('guru.create');
    Route::post('/', 'GuruController@store')->name('guru.store');
    Route::get('/edit/{nip}', 'GuruController@edit')->name('guru.edit');
    Route::post('/update/{nip}', 'GuruController@update')->name('guru.update');
    Route::post('/delete/{nip}', 'GuruController@destroy')->name('guru.destroy');
});

Route::prefix('mapel')->group(function () {
    Route::get('/', 'MapelController@index')->name('mapel.index');
    Route::get('/create', 'MapelController@create')->name('mapel.create');
    Route::post('/', 'MapelController@store')->name('mapel.store');
    Route::get('/edit/{id_mapel}', 'MapelController@edit')->name('mapel.edit');
    Route::post('/update/{id_mapel}', 'MapelController@update')->name('mapel.update');
    Route::post('/delete/{id_mapel}', 'MapelController@destroy')->name('mapel.destroy');
});

Route::prefix('rapot')->group(function () {
    Route::get('/', 'RaportController@index')->name('raport.index');
});

Route::prefix('kurikulum')->group(function () {
    Route::get('/', 'KurikulumController@index')->name('kurikulum.index');
    Route::get('/create', 'KurikulumController@create')->name('kurikulum.create');
    Route::post('/', 'KurikulumController@store')->name('kurikulum.store');
    Route::get('/edit/{id_kuri}', 'KurikulumController@edit')->name('kurikulum.edit');
    Route::post('/update/{id_kuri}', 'KurikulumController@update')->name('kurikulum.update');
    Route::post('/delete/{id_kuri}', 'KurikulumController@destroy')->name('kurikulum.destroy');
});

Route::prefix('siswa')->group(function () {
    Route::get('/', 'SiswaController@index')->name('siswa.index');
    Route::get('/create', 'SiswaController@create')->name('siswa.create');
    Route::post('/', 'SiswaController@store')->name('siswa.store');
    Route::get('/edit/{nisn}', 'SiswaController@edit')->name('siswa.edit');
    Route::post('/update/{nisn}', 'SiswaController@update')->name('siswa.update');
    Route::post('/delete/{nisn}', 'SiswaController@destroy')->name('siswa.destroy');
});

Route::get('/spp', function () {
    return view('spp/index');
});

Route::get('/staff', function () {
    return view('staff/index');
});

Route::get('/users', function () {
    return view('users/index');
});
