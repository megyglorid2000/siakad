<?php

namespace App\Http\Controllers;
use App\Models\Kurikulum;
use Haruncpi\LaravelIdGenerator\IdGenerator;

use Illuminate\Http\Request;

class KurikulumController extends Controller
{
    public function index(Request $request)
    {
        $kurikulum = Kurikulum::get();
        return view('kurikulum.index', compact('kurikulum'));
    }

    public function create()
    {
        $kurikulum = Kurikulum::get();
        return view('kurikulum.create',compact('kurikulum'));
    }

    public function store(Request $request)
    {
        $id = IdGenerator::generate(['table' => 'kurikulum', 'field' => 'id_kuri', 'length' => 10, 'prefix' =>'KRK-']);

        $request->validate([

            'tahun_ajaran' => 'required',

        ]);

        $kurikulum = new Kurikulum;
        $kurikulum->id_kuri = $id;
        $kurikulum->tahun_ajaran = $request->tahun_ajaran;
        $kurikulum->semester = $request->semester;
        $kurikulum->save();

        session()->flash('success','Sukses tambah Kurikulum!');
        return redirect()->route('kurikulum.index');
    }

    public function edit(Request $request, $id_kuri){
        $kurikulum = Kurikulum::findorFail($id_kuri);
        return view('kurikulum.edit', compact('kurikulum', 'kurikulum'));
    }

    public function update(Request $request, $id_kuri){
        $kurikulum = Kurikulum::find($id_kuri);

        $request->validate([

            // 'id_kuri' => 'required',
            // 'nama_kurikulum' => 'required',

        ]);

        $kurikulum -> id_kuri = $request->id_kuri;
        $kurikulum -> tahun_ajaran = $request->tahun_ajaran;
        $kurikulum-> semester = $request->semester;

        $kurikulum -> update();
        // session()->flash('success','Sukses edit Kurikulum!');
        return redirect()->route('kurikulum.index');
    }

    public function destroy($id_kuri){
        $kurikulum = Kurikulum::find($id_kuri);
        $kurikulum -> delete();
        // session()->flash('success','Sukses Hapus Kurikulum!');
        return redirect()->back();
    }
}
