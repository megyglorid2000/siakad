<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    protected $table = 'siswa';

    protected $primaryKey = 'nisn';
    public $incrementing = false;
    protected $fillable = ['nisn','nama', 'jenis_kelamin', 'tempat_lahir', 'tanggal_lahir', 'alamat', 'telepon','agama','nama_ayah','nama_ibu'];
}
