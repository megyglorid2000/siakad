<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Siswa;
class SiswaController extends Controller
{
    public function index(Request $request) {
        $siswa= Siswa::get();
        return view('siswa.index', compact('siswa'));
    }

    public function create(){
        return view('siswa.add');
    }

    public function store(Request $request)
    {

        $siswa = new Siswa;
        $siswa->nisn = $request->nisn;
        $siswa->nama = $request->nama;
        $siswa->jenis_kelamin = $request->jenis_kelamin;
        $siswa->tempat_lahir = $request->tempat_lahir;
        $siswa->tanggal_lahir = $request->tanggal_lahir;
        $siswa->alamat = $request->alamat;
        $siswa->telepon = $request->telepon;
        $siswa->agama = $request->agama;
        $siswa->nama_ayah = $request->nama_ayah;
        $siswa->nama_ibu = $request->nama_ibu;
        $siswa->save();
        session()->flash('success','Sukses tambah Siswa!');
        return redirect()->route('siswa.index');
    }
    public function edit(Request $request, $nisn){
        $siswa = Siswa::findorFail($nisn);
        return view('siswa.edit', compact('siswa'));
    }

    public function update(Request $request, $nisn){
        $siswa = Siswa::find($nisn);

        // $request->validate([
        //     'nisn' => 'required'
        // ]);

        $siswa->nisn = $request->nisn;
        $siswa->nama = $request->nama;
        $siswa->jenis_kelamin = $request->jenis_kelamin;
        $siswa->tempat_lahir = $request->tempat_lahir;
        $siswa->tanggal_lahir = $request->tanggal_lahir;
        $siswa->alamat = $request->alamat;
        $siswa->telepon = $request->telepon;
        $siswa->agama = $request->agama;
        $siswa->nama_ayah = $request->nama_ayah;
        $siswa->nama_ibu = $request->nama_ibu;
        $siswa -> update();
        session()->flash('success','Sukses edit Siswa!');
        return redirect()->route('siswa.index');
    }
    public function destroy($nisn){
        $siswa = Siswa::find($nisn);
        $siswa -> delete();
        session()->flash('success','Sukses Hapus Siswa!');
        return redirect()->back();
    }
}
