@extends('layout.master')
@section('title', 'Siakad | Kelas')
@section('content')
<div class="page-header">
    <h3 class="page-title">
        <span class="page-title-icon bg-gradient-primary text-white mr-2">
            <i class="mdi mdi-account-card-details menu-icon"></i>
        </span> Kelas
    </h3>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Kelas</a></li>
            <li class="breadcrumb-item active" aria-current="page">Home</li>
        </ol>
    </nav>
</div>
<div class="grid-margin stretch-card">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Data Kelas</h4>
        </div>
        <div class="card-body">
            <a class='btn btn-info  btn-gradient-info' href="{{ route('kelas.create') }}"><i
                    class='mdi mdi-plus menu-icon'></i>
                Tambah Kelas</a>
            <br><br>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th> No </th>
                        <th> Kode Kelas </th>
                        <th> Nama Kelas </th>
                        <th> Aksi </th>
                    </tr>
                </thead>
                @foreach ($kelas as $kelas)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $kelas->id_kelas }}</td>
                    <td>{{ $kelas->nama_kelas }}</td>
                    <td>
                        <form action="{{ route('kelas.destroy', $kelas->id_kelas) }}" method="post">
                            <a href="{{ route('kelas.edit', $kelas->id_kelas) }}" class='btn btn-warning  btn-sm'><i
                                    class="mdi mdi-table-edit"></i> </a>
                            @csrf
                            {{-- @method('DELETE') --}}
                            <button type="submit" class='btn btn-danger  btn-sm'><i
                                    class="mdi mdi-delete "></i></button>
                        </form>
                    </td>
                </tr>
                @endforeach
                @forelse($kelas as $kelas)
                @empty
                <tr class='text-center'>
                    <td colspan="6">Tidak ada data</td>
                </tr>
                @endforelse
            </table>
        </div>
    </div>
</div>
@endsection
@section('script')
@if(Session::has('success'))
<script>
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })

    Toast.fire({
        icon: 'success',
        title: 'Data Berhasil Ditambah'
    })
</script>
@endif
@endsection
