@extends('layout.master')
@section('title', 'Siakad | Kurikulum')

@section('content')
<div class="page-header">
    <h3 class="page-title">
        <span class="page-title-icon bg-gradient-primary text-white mr-2">
            <i class="mdi mdi-account-card-details menu-icon"></i>
        </span> Tambah Data Kurikulum
    </h3>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Kurikulum</a></li>
            <li class="breadcrumb-item active" aria-current="page">Tambah Data Kurikulum</li>
        </ol>
    </nav>
</div>

<div class="grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <form action="{{ route('kurikulum.store') }}" method="post" enctype='multipart/form-data'>
                @csrf
                <div class="row">
                  <div class="col-md-6">
                      <div class="form-group">
                          <label>Tahun Ajaran</label>
                          <input class="form-control" autocomplete="off" name="tahun_ajaran" value="{{ old('tahun_ajaran') }}">
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="form-group">
                          <label>Semester</label>
                          <select class="form-control" name="semester" id="example-text-input">
                            <option value="">Pilih Semester</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                          </select>
                      </div>
                  </div>
              </div>
                <button type="submit" class="btn btn-gradient-primary mr-2 float-right">Submit</button>
        </div>
    </div>
    </form>
</div>
@endsection()
@section('script')
<script>
    var picker = new Pikaday({
        field: document.getElementById('datepicker'),
        format: 'D/MM/YYYY',
        toString(date, format) {
            // you should do formatting based on the passed format,
            // but we will just return 'D/M/YYYY' for simplicity
            const day = date.getDate();
            const month = date.getMonth() + 1;
            const year = date.getFullYear();
            return `${day}/${month}/${year}`;
        }
    });

</script>
@endsection
