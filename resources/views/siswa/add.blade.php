@extends('layout.master')
@section('title', 'Siswa | Tambah')
@section('content')
<div class="page-header">
    <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white mr-2">
        <i class="mdi mdi-file-document-box"></i>
      </span> Siswa
    </h3>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Siswa</a></li>
          <li class="breadcrumb-item active" aria-current="page">Tambah Data</li>
        </ol>
      </nav>
  </div>

  <div class="grid-margin stretch-card">
    <div class="card">
        <div class="card-header">
          <h4 class="card-title">Tambah Data Siswa</h4>
        </div>
        <div class="card-body">
            <form action="{{ route('siswa.store') }}" method="post" enctype='multipart/form-data'>
                @csrf
            <div class="form-group row">
                <label for="exampleInputUsername2" class="col-sm-3 col-form-label">NISN</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="nisn" id="exampleInputUsername2" placeholder="Masukan NISN">
                </div>
              </div>

            <div class="form-group row">
                <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Nama</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="nama" id="exampleInputUsername2" placeholder="Masukan Nama Mata Pelajaran">
                </div>
            </div>

            <div class="form-group row">
                <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Jenis Kelamin</label>
                <div class="col-sm-9">
                    <select class="col-sm-5 form-control" name="jenis_kelamin" id="example-text-input">
                        <option selected disabled value>Pilih Jenis Kelamin</option>
                    <option value="Laki - Laki">Laki - Laki</option>
                    <option value="Perempuan">Perempuan</option>
                    </select>
                </div>
            </div>


            <div class="form-group row">
                <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Tempat Lahir</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="tempat_lahir" id="exampleInputUsername2" placeholder="Masukan Tempat Lahir">
                </div>
            </div>

            <div class="form-group row">
                <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Tgl Lahir</label>
                <div class="col-sm-9">
                  <input type="date" class="form-control" name="tanggal_lahir" id="exampleInputUsername2" placeholder="Masukan Tanggal Lahir">
                </div>
            </div>

            <div class="form-group row">
                <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Alamat</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="alamat" id="exampleInputUsername2" placeholder="Masukan Alamat">
                </div>
            </div>

            <div class="form-group row">
                <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Telepon</label>
                <div class="col-sm-9">
                  <input type="number" class="form-control" name="telepon" id="exampleInputUsername2" placeholder="Masukan Telepon">
                </div>
            </div>

            <div class="form-group row">
                <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Agama</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="agama" id="exampleInputUsername2" placeholder="Masukan Agama">
                </div>
            </div>

            <div class="form-group row">
                <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Nama Ayah</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="nama_ayah" id="exampleInputUsername2" placeholder="Masukan Nama Ayah">
                </div>
            </div>

            <div class="form-group row">
                <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Nama Ibu</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="nama_ibu" id="exampleInputUsername2" placeholder="Masukan Nama Ibu">
                </div>
            </div>

        </div>
        <div class="card-footer">
            <button type="submit" class='btn btn-success float-right'>Tambah</button>
        </div>
    </div>
</form>
  </div>
@endsection
