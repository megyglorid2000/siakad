@extends('layout.master')
@section('title', 'Siakad | Mapel')
@section('content')
<div class="page-header">
    <h3 class="page-title">
        <span class="page-title-icon bg-gradient-primary text-white mr-2">
            <i class="mdi mdi-account-card-details menu-icon"></i>
        </span> Mapel
    </h3>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Mapel</a></li>
            <li class="breadcrumb-item active" aria-current="page">Home</li>
        </ol>
    </nav>
</div>
<div class="grid-margin stretch-card">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Data Mapel</h4>
        </div>
        <div class="card-body">
            <a class='btn btn-info  btn-gradient-info' href="{{ route('mapel.create') }}"><i
                    class='mdi mdi-plus menu-icon'></i>
                Tambah Mapel</a>
            <br><br>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th> No </th>
                        <th> Kode Mapel </th>
                        <th> Nama Mapel </th>
                        <th> Nama Pengajar </th>
                        <th> Aksi </th>
                    </tr>
                </thead>
                @foreach ($mapel as $mapel)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $mapel->id_mapel }}</td>
                    <td>{{ $mapel->nama_mapel }}</td>
                    <td>{{ $mapel->guru->nama_guru }}</td>
                    <td>
                        <form action="{{ route('mapel.destroy', $mapel->id_mapel) }}" method="post">
                            <a href="{{ route('mapel.edit', $mapel->id_mapel) }}" class='btn btn-warning  btn-sm'><i
                                    class="mdi mdi-table-edit"></i> </a>
                            @csrf
                            {{-- @method('DELETE') --}}
                            <button type="submit" class='btn btn-danger  btn-sm'><i
                                    class="mdi mdi-delete "></i></button>
                        </form>
                    </td>
                </tr>
                @endforeach
                @forelse($mapel as $mapel)
                @empty
                <tr class='text-center'>
                    <td colspan="6">Tidak ada data</td>
                </tr>
                @endforelse
            </table>
        </div>
    </div>
</div>
@endsection
@section('script')
@if(Session::has('success'))
<script>
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })

    Toast.fire({
        icon: 'success',
        title: 'Data Berhasil Ditambah'
    })
</script>
@endif
@endsection
