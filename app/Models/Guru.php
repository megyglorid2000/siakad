<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Guru extends Model
{
    protected $table = 'guru';

    protected $primaryKey = 'nip';
    protected $fillable = ['nip','nama_guru', 'jenis_kelamin', 'tempat_lahir', 'tanggal_lahir', 'alamat',
'telepon','agama','pendidikan'];

public function mapel(){
    return $this -> hashMany(Mapel::class);
}
}