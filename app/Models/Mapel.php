<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mapel extends Model
{
    protected $table = 'mapel';

    protected $primaryKey = 'id_mapel';

    protected $fillable = ['nama_mapel', 'nip'];
    public $incrementing = false;

    public function guru(){
        return $this -> belongsTo(Guru::class, 'nip');
    }

}
