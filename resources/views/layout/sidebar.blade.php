<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">

      <li class="nav-item">
        <a class="nav-link" href="index.html">
          <span class="menu-title">Dashboard</span>
          <i class="mdi mdi-home menu-icon"></i>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="">
          <span class="menu-title">Users</span>
          <i class="mdi mdi-account-multiple menu-icon"></i>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="">
          <span class="menu-title">Staff</span>
          <i class="mdi mdi-account-network menu-icon"></i>
        </a>
      </li>

      <li class="nav-item @if (Request::segment(1)=='guru' ) nav-item-active active @endif">
        <a class="nav-link" href="{{ route('guru.index') }}">
          <span class="menu-title">Guru</span>
          <i class="mdi mdi-account-card-details menu-icon"></i>
        </a>
      </li>

      <li class="nav-item @if (Request::segment(1)=='siswa' ) nav-item-active active @endif">
        <a class="nav-link" href="{{ route('siswa.index') }}">
          <span class="menu-title">Siswa</span>
          <i class="mdi mdi-account-box menu-icon"></i>
        </a>
      </li>

      <li class="nav-item @if (Request::segment(1)=='kelas' ) nav-item-active active @endif">
        <a class="nav-link" href="{{ route('kelas.index') }}">
          <span class="menu-title">Kelas</span>
          <i class="mdi mdi-bank menu-icon"></i>
        </a>
      </li>

      <li class="nav-item @if (Request::segment(1)=='mapel' ) nav-item-active active @endif">
        <a class="nav-link" href="{{ route('mapel.index') }}">
          <span class="menu-title">Mata Pelajaran</span>
          <i class="mdi mdi-file-document-box
          menu-icon"></i>
        </a>
      </li>

      <li class="nav-item @if (Request::segment(1)=='kurikulum' ) nav-item-active active @endif">
        <a class="nav-link" href="{{ route('kurikulum.index') }}">
          <span class="menu-title">Kurikulum</span>
          <i class="mdi mdi-apps-box
          menu-icon"></i>
        </a>
      </li>

      <li class="nav-item @if (Request::segment(1)=='raport' ) nav-item-active active @endif">
        <a class="nav-link" href="{{ route('raport.index') }}">
          <span class="menu-title">Raport</span>
          <i class="mdi mdi-book-multiple-variant
          menu-icon"></i>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="#">
          <span class="menu-title">SPP</span>
          <i class="mdi mdi-book-open
          menu-icon"></i>
        </a>
      </li>


    </ul>
  </nav>
