@extends('layout.master')
@section('title', 'Siakad | Siswa')
@section('content')
@if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
                @endif
<div class="page-header">
    <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white mr-2">
        <i class="mdi mdi-account-box menu-icon"></i>
      </span> Siswa
    </h3>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Siswa</a></li>
          <li class="breadcrumb-item active" aria-current="page">Home</li>
        </ol>
      </nav>
  </div>

    <div class="grid-margin stretch-card">
      <div class="card">
          <div class="card-header">
            <h4 class="card-title">Data Siswa</h4>
          </div>
        <div class="card-body">
            <a class='btn btn-info  btn-gradient-info' href="{{ route('siswa.create') }}"><i class='mdi mdi-plus menu-icon'></i>
                Tambah Siswa</a>
                <br><br>
            <table class="table table-bordered">
                <thead>

                    <tr>
                        <th> No </th>
                        <th> NISN </th>
                        <th> Nama  </th>
                        <th> Jenis Kelamin </th>
                        <th> Tempat Lahir </th>
                        <th> Tgl Lahir </th>
                        <th> Alamat </th>
                        <th> Telpon </th>
                        {{-- <th> Agama </th>
                        <th> Nama Ayah </th>
                        <th> Nama Ibu </th> --}}
                        <th> Aksi </th>
                      </tr>
                </thead>
                @foreach ($siswa as $siswa)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $siswa->nisn }}</td>
                    <td>{{ $siswa->nama }}</td>
                    <td>{{ $siswa->jenis_kelamin }}</td>
                    <td>{{ $siswa->tempat_lahir }}</td>
                    <td>{{ $siswa->tanggal_lahir }}</td>
                    <td>{{ $siswa->alamat }}</td>
                    <td>{{ $siswa->telepon }}</td>
                    {{-- <td>{{ $siswa->agama }}</td>
                    <td>{{ $siswa->nama_ayah }}</td>
                    <td>{{ $siswa->nama_ibu }}</td> --}}
                    <td>
                        <form action="{{ route('siswa.destroy', $siswa->nisn) }}" method="post">
                            <a href="{{ route('siswa.edit', $siswa->nisn) }}" class='btn btn-warning  btn-sm'><i
                                    class="mdi mdi-table-edit"></i> </a>
                            @csrf
                            {{-- @method('DELETE') --}}
                            <button type="submit" class='btn btn-danger  btn-sm'><i class="mdi mdi-delete "></i></button>
                        </form>
                    </td>
                  </tr>
                  @endforeach
                    @forelse($siswa as $siswa)
                    @empty
                    <tr class='text-center'>
                        <td colspan="6">Tidak ada data</td>
                    </tr>
                    @endforelse


              </table>
        </div>

      </div>
    </div>


@endsection
