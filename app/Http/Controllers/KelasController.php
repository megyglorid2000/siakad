<?php

namespace App\Http\Controllers;
use App\Models\Kelas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Haruncpi\LaravelIdGenerator\IdGenerator;

class KelasController extends Controller
{
    public function index(Request $request)
    {
        $kelas = Kelas::orderBy('nama_kelas', 'asc')->get();
        return view('kelas.index', compact('kelas'));
    }

    public function create()
    {
        $kelas = Kelas::get();
        return view('kelas.create',compact('kelas'));
    }

    public function store(Request $request)
    {
        $id = IdGenerator::generate(['table' => 'kelas', 'field' => 'id_kelas', 'length' => 10, 'prefix' =>'KLS-']);

        $request->validate([

            'nama_kelas' => 'required',

        ]);

        $kelas = new Kelas;
        $kelas->id_kelas = $id;
        $kelas->nama_kelas = "$request->nama_kelas";
        $kelas->save();

        session()->flash('success','Sukses tambah Kelas!');
        return redirect()->route('kelas.index');
    }

    public function edit(Request $request, $id_kelas){
        $kelas = Kelas::findorFail($id_kelas);
        return view('kelas.edit', compact('kelas', 'kelas'));
    }

    public function update(Request $request, $id_kelas){
        $kelas = Kelas::find($id_kelas);

        $request->validate([

            // 'id_kelas' => 'required',
            // 'nama_kelas' => 'required',

        ]);

        $kelas -> id_kelas = $request->id_kelas;
        $kelas -> nama_kelas = $request->nama_kelas;

        $kelas -> update();
        session()->flash('success','Sukses edit Kelas!');
        return redirect()->route('kelas.index');
    }

    public function destroy($id_kelas){
        $kelas = Kelas::find($id_kelas);
        $kelas -> delete();
        // session()->flash('success','Sukses Hapus Kelas!');
        return redirect()->back();
    }

    public function detail_kelas(Request $request){
        $kelas = DB::table('kelas')->get();
    }
}
