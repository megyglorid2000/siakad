<?php

namespace App\Http\Controllers;
use App\Models\Mapel;
use App\Models\Guru;
use Haruncpi\LaravelIdGenerator\IdGenerator;


use Illuminate\Http\Request;

class MapelController extends Controller
{
    public function index(Request $request)
    {
        $mapel = Mapel::get();
        return view('mapel.index', compact('mapel'));
    }
    public function create()
    {
        $mapel = Mapel::get();
        $guru = Guru::get();
        return view('mapel.add',compact('guru', 'mapel'));
    }
    public function store(Request $request)
    {
        $id = IdGenerator::generate(['table' => 'mapel', 'field' => 'id_mapel', 'length' => 10, 'prefix' =>'MPL-']);

        // $request->validate([

        //     'nama_mapel' => 'required',
        //     'nip' => 'required',

        // ]);

        $mapel = new Mapel;
        $mapel->id_mapel = $id;
        $mapel->nama_mapel = $request->nama_mapel;
        $mapel->nip = $request->nip;
        $mapel->save();

        session()->flash('success','Sukses tambah Mapel!');
        return redirect()->route('mapel.index');
    }
    public function edit(Request $request, $id_mapel){
        $mapel = Mapel::findorFail($id_mapel);
        $guru = Guru::get();
        return view('mapel.edit', compact('mapel', 'guru'));
    }
    public function update(Request $request, $id_mapel){
        $mapel = Mapel::find($id_mapel);

        // $request->validate([

        //     'nama_mapel' => 'required',
        //     'nip' => 'required',

        // ]);

        $mapel -> id_mapel = $request->id_mapel;
        $mapel -> nama_mapel = $request->nama_mapel;
        $mapel -> nip = $request->nip;

        $mapel -> update();
        session()->flash('success','Sukses edit Mapel!');
        return redirect()->route('mapel.index');
    }
    public function destroy($id_mapel){
        $mapel = Mapel::find($id_mapel);
        $mapel -> delete();
        session()->flash('success','Sukses Hapus Mapel!');
        return redirect()->back();
    }
}
